const data = require("./data");

// - 1. Write a function called `countAllPeople` which counts the total number of people in `got` variable defined in `data.js` file.

const countAllPeople = () => {
  const houses = data.houses;

  let countofPeople = 0;

  houses.forEach((element) => {
    const peopleArray = element.people;

    countofPeople += peopleArray.length;
  });

  console.log(`Total Number Of people in house ${countofPeople} `);
};

// countAllPeople();

//   - 2. Write a function called `peopleByHouses` which counts the total number of people in different houses in the `got` variable defined in `data.js` file.

const PeopleByHouse = () => {
  const houses = data.houses;

  const PeopleByHousedata = houses.reduce((accumulator, currentValue) => {
    if (!accumulator.hasOwnProperty(currentValue.name)) {
      accumulator[currentValue.name] = currentValue.people.length;
    }

    return accumulator;
  }, {});

  console.log(PeopleByHousedata);
};

// PeopleByHouse();

//   - 3. Write a function called `everyone` which returns a array of names of all the people in `got` variable.

const Everyone = () => {
  const houses = data.houses;

  const everyOneData = houses.reduce((accumulator, currentValue) => {
    currentValue.people.forEach((element) => {
      accumulator.push(element.name);
    });

    return accumulator;
  }, []);

  console.log(everyOneData);
};
Everyone();

//   - 4. Write a function called `nameWithS` which returns a array of names of all the people in `got` variable whose name includes `s` or `S`.

const nameWithS = () => {
  const houses = data.houses;

  const nameWithSNames = houses.reduce((accumulator, currentValue) => {
    let peopleArray = currentValue.people;

    peopleArray.forEach((element) => {
      if (element.name.includes("s") || element.name.includes("S")) {
        accumulator.push(element.name);
      }
    });

    return accumulator;
  }, []);

  console.log(nameWithSNames);
};

// nameWithS();

//   - 5. Write a function called `nameWithA` which returns a array of names of all the people in `got` variable whose name includes `a` or `A`.

const nameWithA = () => {
  const houses = data.houses;

  const nameWithANames = houses.reduce((accumulator, currentValue) => {
    let peopleArray = currentValue.people;

    peopleArray.forEach((element) => {
      if (element.name.includes("A") || element.name.includes("a")) {
        accumulator.push(element.name);
      }
    });

    return accumulator;
  }, []);

  console.log(nameWithANames);
};

// console.log(nameWithA());

//   - 6. Write a function called `surnameWithS` which returns a array of names of all the people in `got` variable whoes surname is starting with `S`(capital s).

const surnameWithS = () => {
  const houses = data.houses;

  const surnameWithS = houses.reduce((accumulator, currentValue) => {
    let peopleArray = currentValue.people;

    peopleArray.forEach((element) => {
      let NameArray = element.name.split(" ");
      const surname = NameArray[1];
      if (surname.includes("S")) {
        accumulator.push(element.name);
      }
    });

    return accumulator;
  }, []);

  console.log(surnameWithS);
};

// surnameWithS();

//   - 7. Write a function called `surnameWithA` which returns a array of names of all the people in `got` variable whoes surname is starting with `A`(capital a).

const surnameWithA = () => {
  const houses = data.houses;

  const surnameWithA = houses.reduce((accumulator, currentValue) => {
    let peopleArray = currentValue.people;

    peopleArray.forEach((element) => {
      let NameArray = element.name.split(" ");
      const surname = NameArray[1];
      if (surname.includes("A")) {
        accumulator.push(element.name);
      }
    });

    return accumulator;
  }, []);

  console.log(surnameWithA);
};

// surnameWithA();

//   - 8. Write a function called `peopleNameOfAllHouses` which returns an object with the key of the name of house and value will be all the people in the house in an array.

const peopleNameOfAllHouses = () => {
  const houses = data.houses;

  const peopleAllHouse = houses.reduce((accumulator, currentValue) => {
    if (!accumulator.hasOwnProperty(currentValue.name)) {
      let namesArray = [];

      currentValue.people.forEach((element) => {
        namesArray.push(element.name);
      });

      if (!accumulator[currentValue.name]) {
        accumulator[currentValue.name] = namesArray;
      }
    }

    return accumulator;
  }, {});

  console.log(peopleAllHouse);
};

// peopleNameOfAllHouses();
